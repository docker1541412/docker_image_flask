# Первый этап: сборка
FROM python:3.8-alpine AS builder

# Обновляем apk и устанавливаем зависимости, нужные только на этапе сборки
RUN apk add --no-cache \
    postgresql-dev \
    gcc \
    musl-dev \
    libffi-dev

# Создаем рабочую директорию
WORKDIR /app

# Копируем файлы requirements.txt в контейнер
COPY requirements.txt .

# Устанавливаем зависимости из requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Копируем все файлы приложения в контейнер
COPY . .

# Инициализация и установка тестовых данных в базу данных
RUN flask db upgrade
RUN python seed.py

# Второй этап: финальный образ
FROM python:3.8-alpine

# Устанавливаем необходимые зависимости для запуска
RUN apk add --no-cache \
    postgresql-libs

# Создаем рабочую директорию
WORKDIR /app

# Копируем установленные зависимости из этапа сборки
COPY --from=builder /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages
COPY --from=builder /usr/local/bin /usr/local/bin

# Если ваш проект зависит от файлов, созданных в результате работы на стадии сборки, их тоже нужно скопировать:
COPY --from=builder /app /app

# Опционально: Установка переменной окружения DATABASE_URL, если требуется
# ENV DATABASE_URL=<ваша SQLAlchemy строка подключения>

# Expose порт, на котором будет работать ваше приложение Flask (по умолчанию 5000)
EXPOSE 5000

# Указываем команду по умолчанию для запуска приложения Flask
CMD ["flask", "run", "--host=0.0.0.0"]


